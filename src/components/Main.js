import React from 'react';
import NavButton from '../components/NavButton';

const Main = () => {
  return (
    <section>
      <div className="min-w-full h-screen bg-center py-20">
        <div className="w-full h-full">
          <div className="container m-auto h-full flex items-center content-center">
            <div className="flex w-full">
              <div className="flex flex-col-reverse md:flex-row lg:flex-row xl:flex-row items-center w-full">
                <div className="w-full md:w-5/12 lg:w-5/12 xl:w-5/12 h-full flex flex-col justify-center px-6">
                  <h1 className="text-5xl lg:text-4xl xl:text-6xl text-white" style={{fontFamily:`Raleway`, fontWeight:700}}>Hyke Up <br/> Your Skills</h1>
                  <p className="text-s md:text-base lg:text-bas xl:test-base text-white leading-loose mt-6" style={{fontFamily:`Raleway`}}>Looking for something to <b>level up</b> your technical skills? You've found the right place. Hyke, your go-to platform to learn <b>Tech</b> in a fun way.</p>
                  <div className="mt-10">
                    <NavButton destination='/course' buttonName='Get Started' />
                  </div>
                </div>
                <div className="w-3/4 hidden md:w-7/12 lg:w-7/12 xl:w-7/12 md:flex justify-end items-center">
                  <img src={require("../assets/main.svg")} className="w-10/12 z-9" alt="working together"/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <canvas id="playArea"></canvas>
    </section>
  )
}

export default Main