import React from 'react';
import SectionHeader from './SectionHeader';

const Why = () => {
  return (
    <section className="min-w-full bg-center py-24 background-white">
      <div className="container m-auto flex justify-center align-center px-6 flex-col md:flex-row">
        <div className="w-full md:w-6/12 h-full">
        <img src={require("../assets/why.svg")} className="w-11/12 z-9" alt="working together"/>
        </div>
        <div className="w-full md:w-6/12 h-full">
          <div className="flex flex-col">
            <SectionHeader name='WHY HYKE'/>
            <h3 className="font-normal text-l md:text-xl leading-loose">There are <b>too many</b> courses and academies out there scamming <strong>beginners and new learners</strong> for their money. My mission is to equip people with actual <strong>transferable skills</strong> at an <strong>affordable price</strong>. I make things as <b>easy</b> and as <b>fun</b> as possible.</h3>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Why