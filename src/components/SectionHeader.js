import React from 'react';

const SectionHeader = (props) => {
  return <h2 className="font-semibold text-xl tracking-loose  my-12" style={{color: "#b1bdd7"}}>{props.name}</h2>
}

export default SectionHeader;